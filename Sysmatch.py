def combinationSum(candidates, target):
        result = list()
        candidates.sort()
        _combinationSum(candidates, target, 0, list(), result)
        return result


def _combinationSum(nums, target, index, path, res):
        if target <= 0:
            res.append(path)
            return

        if path and target < path[-1]:
            return

        for i in range(index, len(nums)):
            _combinationSum(nums, target - nums[i], i, path + [nums[i]], res)