from flask import Flask, request
# from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import requests
import json
import math

from Temperature import Temperature
from Strategy import Strategy
from Usermatch import Usermatch
import Sysmatch
from models import *

app = Flask(__name__)

# 设置最适宜温度
ProperTemp = 26
# 设置数据库
HOSTNAME = '127.0.0.1'
PORT = '3306'
DATABASE = 'Weatherobe'
USERNAME = 'root'
PASSWORD = 'Weather0be'
DB_URI = "mysql+pymysql://{username}:{password}@{host}:{port}/{db}?charset=utf8".format(username=USERNAME,
                                                                                        password=PASSWORD,
                                                                                        host=HOSTNAME, port=PORT,
                                                                                        db=DATABASE)

# app.config['SQLALCHEMY_DATABASE_URI'] = DB_URI
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# db = SQLAlchemy(app)

engine = create_engine(DB_URI)
Session = sessionmaker(bind=engine)


@app.route('/')
def hello():
    return 'Hello World!'


# 获取用户openid
@app.route('/id', methods=['POST'])
def break_id():
    data = request.get_json()
    code = data['code']
    app_id = 'wx6c0312f53346535d'
    app_key = '4a2253bf6fe7b165c276f7717a17263a'

    req_url = 'https://api.weixin.qq.com/sns/jscode2session?appid=' + \
              app_id + '&secret=' + app_key + '&js_code=' + code + '&grant_type=authorization_code'
    req_id = requests.get(req_url)
    req_pak = req_id.json()
    u_id = req_pak['openid']
    # print(u_id)
    cod_pak = {
        'openid': u_id
    }
    cod_snd = json.dumps(cod_pak)
    # return info
    return cod_snd


# 获取用户信息
@app.route('/usr', methods=['POST'])
def get_usr_param():
    # process parameters
    req_data = request.get_json()
    u_id = req_data['openID']
    u_name = req_data['userName']
    u_sex = req_data['userGender']
    u_pos = req_data['userPos']

    param_list = [u_id, u_name, u_sex, u_pos]

    # print(param_list)
    if query(0, param_list):
        return 'a new user arrived!'
    else:
        return 'hello my friend!'


# 获取用户所有图片
@app.route('/img', methods=['POST'])
def get_usr_img():
    # process parameters
    req_data = request.get_json()
    uid = req_data['openid']

    param_list = [uid]
    img_list = query(1, param_list)

    # return info
    res = json.dumps(img_list)
    return res


@app.route('/live', methods=['POST'])
def get_live_weather():
    # process parameters
    req_data = request.get_json()
    realtimetp = req_data['realtimetp']
    humidity = req_data['humidity']
    windspeed = req_data['windspeed']

    # return info
    return 'live weather data got!'


@app.route('/next6hrs', methods=['POST'])
def get_next6hrs_weather():
    # process parameters
    req_data = request.get_json()
    time1 = req_data['time1']
    time3 = req_data['time3']
    time6 = req_data['time6']

    # get temperature
    time1_tmp = time1['time1_tmp']
    time3_tmp = time3['time3_tmp']
    time6_tmp = time6['time6_tmp']

    # get humidity
    time1_hum = time1['time1_hum']
    time3_hum = time3['time3_hum']
    time6_hum = time6['time6_hum']

    # get wind speed
    time1_wind_spd = time1['time1_wsd']
    time3_wind_spd = time3['time3_wsd']
    time6_wind_spd = time6['time6_wsd']

    # return info
    return 'next 6 hours data got!'


# 返回实时搭配建议
@app.route('/strategy', methods=['POST'])
def get_strategy_param():
    req_data = request.get_json()
    res_data = {}
    list_v1 = []
    list_v2 = []
    list_final = []
    time_add_list = [1, 2, 3, 4, 5, 6, 9]
	
    month = req_data['date'][:2]
    realtimetp = req_data['realtimetp']
    humidity = req_data['humidity']
    windspeed = req_data['windspeed']
    precipit_p = req_data['precipit']
    next6hrs = req_data['next6hrs']

    time1 = next6hrs['time1']
    time3 = next6hrs['time3']
    time6 = next6hrs['time6']

    time1_tmp = float(time1['time1_tmp'])
    time3_tmp = float(time3['time3_tmp'])
    time6_tmp = float(time6['time6_tmp'])
    time1_hum = float(time1['time1_hum'])
    time3_hum = float(time3['time3_hum'])
    time6_hum = float(time6['time6_hum'])
    time1_wind_spd = float(time1['time1_wsd'])
    time3_wind_spd = float(time3['time3_wsd'])
    time6_wind_spd = float(time6['time6_wsd'])

    # 获取体感温度
    s_bdtp = Temperature(int(realtimetp), int(windspeed), int(humidity))
    s_result1 = s_bdtp.calculate_bd()
	
    temp_diff = ProperTemp - s_result1
    if int(month) == 12 or 1 or 2 or 3 or 4 or 5:
        temp_diff = math.ceil(temp_diff)
    else:
        temp_diff = math.floor(temp_diff)
    
    # 温差大于0度，调用Sysmatch
    if temp_diff > 0:
        temp_add = Sysmatch.combinationSum(time_add_list, temp_diff)
        item_num = len(temp_add)
		# toptemp_dic = {1: '背心/T恤', 2: '薄卫衣/针织毛衣/保暖内衣/衬衫',
		#                3: '厚卫衣/薄外套', 4: '毛衣/风衣 ',
		#                5: '大衣/棉服', 6: '薄款羽绒服', 9: '厚款羽绒服'}
    
		# 有5,6,9三种的必须有1 2 3 4中任意一个或多个
		# 1 只能有一个
		# 夏季只能有 1 2 3
        for num in range(0, item_num):
            temp_add[num].sort()
            count_1 = 0
            for item in temp_add[num]:
                if item == 1:
                    count_1 = count_1 + 1
            if count_1 <= 1:
                list_v1.append(temp_add[num])
    # print(list_v1)
    # 已经按从小到大排列
    # 检测第一个是否为 1,2,3,4
    # 如果是，则保留
	# list_v1 = [[]]
        item_num = len(list_v1)
        for num in range(0, item_num):
            i = list_v1[num]
            if i[0] <= 4:
                list_v2.append(i)
        
        item_num = len(list_v2)
        if int(month) == 6 or 7 or 8:
            for num in range(0, item_num):
                i = list_v1[num]
                if i[-1] <= 3:
                    list_final.append(i)

        if len(list_final) == 0:
            list_final.append([])
    # 温差小于0度
    else:
        list_final.append =([])


    # 计算六小时体感温度
    # 并生成列表
    trend_bdtp1 = Temperature(time1_tmp, time1_wind_spd, time1_hum)
    trend_bdtp3 = Temperature(time3_tmp, time3_wind_spd, time3_hum)
    trend_bdtp6 = Temperature(time6_tmp, time6_wind_spd, time6_hum)
    trend_result1 = trend_bdtp1.calculate_bd()
    trend_result3 = trend_bdtp3.calculate_bd()
    trend_result6 = trend_bdtp6.calculate_bd()
    next_temp = [trend_result1, trend_result3, trend_result6]

    # 获取系统建议信息
    strategy = Strategy(str(int(month)), int(precipit_p), list_final, next_temp)
    # res_data = {}
    res_data['str1'] = strategy.match_strategy()
    res_data['str2'] = strategy.prcipitp_strategy()
    res_data['str3'] = strategy.trend_strategy()
    res = json.dumps(res_data)

    return res


# 用户上传服装图片
@app.route('/upload', methods=['POST'])
def get_usr_upload():
    # process parameters
    req_data = request.get_json()
    u_id = req_data['openid']
    u_cos_class = req_data['cosclass']
    u_cos_url = req_data['cosurl']
    cos_upl_time = req_data['datetime']

    # 服装ID生成
    cos_id, str_time = [], []
    if int(u_cos_class) in range(1, 14):
        cos_id.append('tp')
    elif int(u_cos_class) in range(14, 17):
        cos_id.append('bt')
    else:
        return 'class error!'
    if int(u_cos_class) in range(1, 10):
        cos_id.append('0')
    cos_id.append(u_cos_class)
    str_p = cos_upl_time.split(' ')
    for elem in str_p[0].split('-'):
        str_time.append(elem)
    for elem in str_p[1].split(':'):
        str_time.append(elem)
    str_time = ''.join(str_time)
    cos_id.append(str_time)
    cos_id = ''.join(cos_id)

    # 存储服装图片
    param_list = [u_id, cos_id, u_cos_class, u_cos_url]
    # print(param_list)
    insert(1, param_list)

    # return info
    return cos_id


# 用户删除服装图片
@app.route('/rm', methods=['POST'])
def get_rm_id():
    # process parameters
    req_data = request.get_json()
    rm_list = req_data
    # print(rm_list)
    for clothesid in rm_list:
        delete(clothesid)
    # return info
    return 'items deleted!'


# 用户搭配
# 系统评价
# 添加历史记录
@app.route('/fit', methods=['POST'])
def get_fit_param():
    # process parameters
    req_data = request.get_json()
    f_uid = req_data['openid']
    f_time = req_data['datetime']
    f_tp1 = req_data['tops1']
    f_tp2 = req_data['tops2']
    f_btm = req_data['bottoms']
    realtimetp = float(req_data['realtimetp'])
    windspeed = float(req_data['windspeed'])
    humidity = float(req_data['humidity'])
    month = str(int(f_time[5:7]))

    # 添加此条搭配的历史记录
    param_list = [f_time, f_tp1, f_tp2, f_btm, f_uid]
    if f_tp1 == '0' and f_tp2 == '0' and f_btm == '0':
        pass
    else:
        insert(2, param_list)

    # return info
    res_data = {}
    # 计算体感温度
    f_bdtp = Temperature(realtimetp, windspeed, humidity)
    f_result1 = f_bdtp.calculate_bd()
    # 计算用户搭配后温度
    f_afbd = Usermatch(f_result1, month, f_tp1, f_tp2)
    # 元组形式储存 Usermatch 的两个返回值
    f_result2 = f_afbd.calculate_afbd_byuser()
    res_data['temp'] = f_result2[0]
    res_data['advice'] = f_result2[1]
    res = json.dumps(res_data)
    return res


# 数据库查询
def query(qtype, param_list):
    session = Session()
    # 查询用户是否存在
    if qtype == 0:
        user = session.query(User).filter_by(WechatID=param_list[0]).all()
        if len(user) == 0:
            insert(0, param_list)
            session.close()
            return 1
        else:
            session.close()
            return 0
    # 查询服装图片信息
    elif qtype == 1:
        toplist = session.query(Top).filter_by(User_WechatID=param_list[0]).all()
        bottomlist = session.query(Bottom).filter_by(User_WechatID=param_list[0]).all()
        imagelist = []
        for top in toplist:
            temp = {}
            temp['id'] = top.TopID
            temp['url'] = top.TopImage
            imagelist.append(temp)
        for bottom in bottomlist:
            temp = {}
            temp['id'] = bottom.BottomID
            temp['url'] = bottom.BottomImage
            imagelist.append(temp)
        session.close()
        return imagelist
    else:
        session.close()
        pass


# 数据库添加
def insert(itype, param_list):
    session = Session()
    # 增加用户
    if itype == 0:
        user_add = User(WechatID=param_list[0], UserName=param_list[1],
                        Gender=param_list[2], Location=param_list[3])
        session.add(user_add)
        session.commit()
    # 上传图片
    elif itype == 1:
        clothesID = param_list[1]
        clothesType = clothesID[0]
        # 上传上装
        if clothesType == 't':
            top_add = Top(TopID=param_list[1], User_WechatID=param_list[0],
                          TopImage=param_list[3], TopType=int(param_list[2]))
            session.add(top_add)
            session.commit()
        # 上传下装
        elif clothesType == 'b':
            bottom_add = Bottom(BottomID=param_list[1], User_WechatID=param_list[0],
                                BottomImage=param_list[3], BottomType=int(param_list[2]))
            session.add(bottom_add)
            session.commit()
        else:
            pass
    # 增加历史记录
    elif itype == 2:
        history_add = History(HistoryTime=param_list[0], User_WechatID=param_list[4],
                              Top1ID=param_list[1], Top2ID=param_list[2], BottomID=param_list[3])
        session.add(history_add)
        session.commit()
    else:
        pass
    session.close()


# 数据库删除
def delete(param):
    session = Session()
    if param[0] == 't':
        topdelete = session.query(Top).filter_by(TopID=param).first()
        session.delete(topdelete)
        session.commit()
    elif param[0] == 'b':
        bottomdelete = session.query(Bottom).filter_by(BottomID=param).first()
        session.delete(bottomdelete)
        session.commit()
    else:
        pass
    session.close()


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5678)