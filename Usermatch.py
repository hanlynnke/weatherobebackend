# 计算用户搭配后的体感温度
# 衣服类别为字符型
# 衣服温度为整型

import math

type_temp_dic = {'0': 0, '1': 1, '2': 1, '3': 2, '4': 2, '5': 2, '6': 2,
                 '7': 3, '8': 3, '9': 4, '10': 4,
                 '11': 5, '12': 5, '13': 6, '14': 9}


class Usermatch:
    def __init__(self, initbd, month, tp_1, tp_2):
        self.initbd = initbd
        self.month = month
        self.clotheschosen_byuser = [tp_1, tp_2]

    def calculate_afbd_byuser(self):
        if self.month == '12' or '1' or '2' or '3' or '4' or '5':
            aftbd = math.floor(self.initbd)
        else:
            aftbd = math.ceil(self.initbd)
        addtemp = []
        for f_tp in self.clotheschosen_byuser:
            addtemp.append(type_temp_dic[f_tp])

        # 循环累加计算用户搭配的最终温度
        for tp_value in addtemp:
            aftbd = aftbd + tp_value
        if aftbd < 25:
            str1 = '多穿点吧'
        elif aftbd > 27:
            str1 = '少穿点吧'
        else:
            str1 = '刚刚好呢'
        return aftbd, str1
