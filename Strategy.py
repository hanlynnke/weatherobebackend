import numpy as np
# from scipy.optimize import leastsq

toptemp_dic = {1: '背心/T恤', 2: '薄卫衣/针织毛衣/保暖内衣/衬衫',
               3: '厚卫衣/薄外套', 4: '毛衣/风衣 ',
               5: '大衣/棉服', 6: '薄款羽绒服', 9: '厚款羽绒服'}


class Strategy:
    def __init__(self, Month, prcipitP, Temp_add, NextTemp):
        self.Temp_add = Temp_add,
        self.NextTemp = NextTemp
        self.prcipitP = prcipitP
        self.Month = Month
        self.Temp_add = self.Temp_add[0]

    def match_strategy(self):
        final_temp_add = []
        clothes_list = []

        if len(self.Temp_add) == 1:
            for item in self.Temp_add:
                if len(item) == 0:
                    clothes_list = [['背心/T恤']]
                else:
                    for elem in item:
                        clothes_list = [[toptemp_dic[elem]]]
        else:
            if self.Month == '12' or '1' or '2':
                for i in self.Temp_add:
                    if len(i) <= 5:
                        final_temp_add.append(i)
                for t in final_temp_add:
                    clothes_add = []
                    for j in t:
                        clothes_add.append(toptemp_dic[j])
                    clothes_list.append(clothes_add)

            elif self.Month == '3' or '4' or '5' or '9' or '10' or '11':
                for i in self.Temp_add:
                    if len(i) <= 3:
                        final_temp_add.append(i)
                for t in final_temp_add:
                    clothes_add = []
                    for j in t:
                        clothes_add.append(toptemp_dic[j])
                    clothes_list.append(clothes_add)

            elif self.Month == '6' or '7' or '8':
                for i in self.Temp_add:
                    if len(i) <= 2:
                        final_temp_add.append(i)
                for t in final_temp_add:
                    clothes_add = []
                    for j in t:
                        clothes_add.append(toptemp_dic[j])
                    clothes_list.append(clothes_add)
            else:
                pass

        return clothes_list

    def prcipitp_strategy(self):
        if self.prcipitP >= 75:
            str2 = ('今天极有可能下雨哦，'
                        '记得携带雨具')
        elif self.prcipitP >= 50 and self.prcipitP < 75:
            str2 = ('今天很有可能下雨哦，'
                        '推荐携带雨具')
        elif self.prcipitP >= 25 and self.prcipitP < 50:
            str2 = ('今天下雨的可能性较小，'
                        '度过美好的一天吧')
        else:
            str2 = ('今天下雨的可能性极小，'
                        '度过美好的一天吧')

        return str2

    def trend_strategy(self):
        # 通过最小二乘法获得温度变化趋势
        # 根据用户刷新时间
        # 计算接下来六个个小时内的温度变化情况
        data_x = self.NextTemp
        data_y = [1,2,3]
        size = len(data_x)
        i = 0
        sum_x = 0
        sum_y = 0
        sum_xy = 0
        sum_sqare_x = 0
        while i < size:
            sum_xy += data_x[i] * data_y[i]
            sum_y += data_y[i]
            sum_x += data_x[i]
            sum_sqare_x += data_x[i] * data_x[i]
            i += 1
        k = (size * sum_xy - sum_x * sum_y) / (size * sum_sqare_x - sum_x * sum_x)
        if k >= 0:
            str3 = '近六个小时会有升温'
        else:
            str3 = '近六个小时会有降温'

        return str3
