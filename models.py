# coding: utf-8
from sqlalchemy import Column, ForeignKey, String, Text, text, create_engine
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base



HOSTNAME = "localhost"
PORT = "3306"
DATABASE = "Weatherobe"
USERNAME = "root"
PASSWORD = "Weather0be"
 
DB_URI = "mysql+pymysql://{username}:{password}@{host}:{port}/{db}?charset=utf8".\
    format(username=USERNAME,password=PASSWORD,host=HOSTNAME,port=PORT,db=DATABASE)
 
engine = create_engine(DB_URI)
 
Base = declarative_base(engine)
metadata = Base.metadata


class User(Base):
    __tablename__ = 'User'

    WechatID = Column(String(30), primary_key=True)
    UserName = Column(String(20), nullable=False)
    Gender = Column(INTEGER(1), nullable=False)
    Location = Column(String(30), nullable=False)


class Bottom(Base):
    __tablename__ = 'Bottom'

    BottomID = Column(String(20), primary_key=True)
    User_WechatID = Column(ForeignKey('User.WechatID', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    BottomImage = Column(Text, nullable=False)
    BottomType = Column(INTEGER(2), nullable=False)

    User = relationship('User')


class History(Base):
    __tablename__ = 'History'

    HistoryTime = Column(String(20), primary_key=True, server_default=text("''"))
    User_WechatID = Column(ForeignKey('User.WechatID'), nullable=False, index=True)
    Top1ID = Column(String(20), nullable=False)
    Top2ID = Column(String(20), nullable=False)
    BottomID = Column(String(20), nullable=False)

    User = relationship('User')


class Top(Base):
    __tablename__ = 'Top'

    TopID = Column(String(20), primary_key=True)
    User_WechatID = Column(ForeignKey('User.WechatID', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    TopImage = Column(Text, nullable=False)
    TopType = Column(INTEGER(2), nullable=False)

    User = relationship('User')
