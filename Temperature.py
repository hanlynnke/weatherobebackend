# 计算体感温度
import numpy as np


class Temperature:
    def __init__(self, realtimetp, windspeed, humidity):
        self.realtimetp = realtimetp  # 实时温度
        self.windspeed = windspeed  # 风速
        self.humidity = humidity  # 湿度

    def calculate_bd(self):
        t = 17.27 * self.realtimetp / (237.7 + self.realtimetp)
        epressure = self.humidity / 100 * 6.105 * np.exp(t)
        # 计算体感温度
        bodytemp = 1.07 * self.realtimetp + 0.2 * epressure - 0.65 * self.windspeed - 2.7
        return bodytemp
